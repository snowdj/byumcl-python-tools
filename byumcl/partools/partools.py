
def _test_par_print(comm):
    """Tests par_print.
    """
    rank = comm.rank
    message = "Hello from process " + str(rank)

    rprint(comm, "Should be this:")
    rprint(comm, "[0]: Hello from process 0 ")
    rprint(comm, "[1]: Hello from process 1 ")
    rprint(comm, "[2]: Hello from process 2 ")
    rprint(comm, "Function gives this:")
    par_print(comm, message)


def _run_tests(comm):
    """Tests the functions in this module.

    Uses the examples given in each function to test the functions
    """
    _test_ordered_balance()
    _test_gatv_scatv_tuples()
    _test_par_print(comm)

if __name__ == "__main__":
    comm = MPI.COMM_WORLD

    #tests make sense when 3 processors are used.
    assert comm.size == 3

    #test all functions
    _run_tests(comm)
