#test_collective_utilities.py
from mpinoseutils import *
import byumcl.partools.collective_utilities as cu

#Notice that I have created a separate class for each function in the
#load_balancing module. I've done this for organizational purposes.

# For syntax in using mpinoseutils, see https://github.com/dagss/mpinoseutils


@mpitest(4)
def test_demo(comm):
    #test if mpinoseutils is working
    assert comm.Get_size() == 4
    allranks = comm.allgather(comm.Get_rank())
    mprint(comm, 'allranks == ', allranks)
    assert_eq_across_ranks(comm, allranks)

@mpitest(5)
def test_demo2(comm):
    #test if mpinoseutils is working
    assert comm.Get_size() == 5
    allranks = comm.allgather(comm.Get_rank())
    mprint(comm, 'allranks == ', allranks)

@mpitest(4) # argument is number of ranks needed for test
def test_demo3(comm):
    #test if mpinoseutils is working
    assert comm.Get_size() == 4

    # meq_: Different expected result for each rank
    meq_(comm, [0, 1, 2, 3], comm.Get_rank())

    allranks = comm.allgather(comm.Get_rank())
    # mprint: collective non-garbled printing
    mprint(comm, 'allranks == ', allranks)
    assert_eq_across_ranks(comm, allranks)

@mpitest(3)
def test_gather_strings(comm):
    rank = comm.rank
    message = "Hello from process " + str(rank)
    gathered_fun = cu.gather_strings(comm, message)

    gathered_man = ["Hello from process 0", "Hello from process 1", 
                "Hello from process 2"]

    meq_(comm, [gathered_man, [], []], gathered_fun)


