"""
byumcl
======

Provides a variety of routines that are useful in solving problems in
computational applied mathematics, with a special emphasis on economics.

The following subpackages are available:

interp
    Routines for constructing interpolating functions and approximations
uhlig
    A python implementation of the popular MatLab toolkit made by Uhlig
matstat
    Various statistical distributions that match MatLab and Mathematica
    in syntax and structure
misc
    Other tools like gauss-hermite quadrature node creation, tools for
    working with latex, and more.

How to use the documentation
----------------------------
Documentation is available both as docstrings for all classes and
functions as well as a compiled version in pdf and html format.
TODO: Give links to the pdf and the html.
"""

version = "0.1"
__docformat__ = 'restructuredtext'

import byumcl.matstat as matstat
import byumcl.interp as interp
import byumcl.uhlig as uhlig
import byumcl.misc as misc
import byumcl.partools as partools
